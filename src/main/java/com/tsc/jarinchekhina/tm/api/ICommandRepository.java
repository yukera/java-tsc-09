package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.model.Command;

public interface ICommandRepository {

    Command[] getConstCommands();

}