package com.tsc.jarinchekhina.tm.api;

public interface ICommandController {

    void showInfo();

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void showErrorCommand();

    void showErrorArgument();

}
