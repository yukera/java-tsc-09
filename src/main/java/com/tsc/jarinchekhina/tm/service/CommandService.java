package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.ICommandService;
import com.tsc.jarinchekhina.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getConstCommands() {
        return commandRepository.getConstCommands();
    }
}
